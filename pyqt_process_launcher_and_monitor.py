#!/usr/bin/python2
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 29 10:59:41 2016

@author: aleksandaratanasov
"""

from sys import argv, exit
from os import kill
from signal import SIGINT

from PyQt4.QtCore import QObject, pyqtProperty, pyqtSlot, pyqtSignal, QTimer, QThread, QProcess
from PyQt4.QtGui import QApplication, QWidget, QLineEdit, QHBoxLayout, QLabel, QPushButton

# More on Q_PROPERTY in PyQt4: pyqt.sourceforge.net/Docs/PyQt4/qt_properties.html

class Worker(QObject):
  class Status:
    inactive = 0
    running = 1
    failed = 2

  statusChanged_signal = pyqtSignal(int)
  block_signal = pyqtSignal(bool)

  def __init__(self, parent=None):
    super(Worker, self).__init__(parent)
    self.stat = -1
    self.active = False
    self.pid = None
    self.cmd = ''
    self.args = []
    self.path = ''

#  def __del__(self):
#    if self.active:
#      if self.pid: kill(self.pid, SIGINT)

  @pyqtSlot(str)
  def setup_cmd_args(self, cmd_and_args):
    raw = cmd_and_args.split(' ', 1)
    if not len(raw): return
    self.cmd = raw[0]
    if len(raw) > 1: self.args = raw[1]

  @pyqtSlot()
  def start(self):
    if not self.active and self.cmd:
      self.block_signal.emit(True)
      self.active, self.pid = QProcess.startDetached(self.cmd, self.args, '/tmp')
      print(self.active, self.pid)

      if not self.active or not self.pid:
        self.setStatus(Worker.Status.failed)
        self.block_signal.emit(False)
        return

      QThread.sleep(5)
      self.setStatus(Worker.Status.running)
      self.block_signal.emit(False)

  @pyqtSlot()
  def stop(self):
    if self.active:
      self.block_signal.emit(True)
      if self.pid:
        kill(self.pid, SIGINT)
        QThread.sleep(5)
      self.active = False
      self.setStatus(Worker.Status.inactive)

      self.block_signal.emit(False)

  @pyqtSlot()
  def status(self):
    if self.active:
      if self.pid:
        proc_status = self.checkPid()
        if not proc_status:
          # Process has died or was closed externally
          self.setStatus(Worker.Status.failed)
          self.active = False
  @pyqtSlot()
  def clear_error(self):
    self.setStatus(Worker.Status.inactive)

  def getStatus(self):
    return self.status

  def setStatus(self, stat):
    print('New status %d' % stat)
    if self.stat == stat: return
    self.stat = stat
    self.statusChanged_signal.emit(self.stat)

  statusProp = pyqtProperty(int, fget=getStatus, fset=setStatus, notify=statusChanged_signal)

  def checkPid(self):
    if not self.pid: return False

    try: kill(self.pid, 0)
    except OSError: return False
    else: return True

class Text(QObject):
  class CommandType:
    cmd_exit = 0
    cmd_hello = 1

  textChanged = pyqtSignal(int)

  def __init__(self, parent=None):
    super(Text, self).__init__(parent)
    self._text = ''

  def getText(self):
    return self._text

  def setText(self, value):
    if self._text == value: return
    self._text = value
    if value == 'exit':
      self.textChanged.emit(Text.CommandType.cmd_exit)
    elif value == 'hello':
      self.textChanged.emit(Text.CommandType.cmd_hello)

  text = pyqtProperty(str, fget=getText, fset=setText, notify=textChanged)

class Example(QWidget):

  setup_signal = pyqtSignal(str)
  start_signal = pyqtSignal()
  stop_signal = pyqtSignal()
  clear_error_signal = pyqtSignal()

  def __init__(self):
    super(Example, self).__init__()

    self.obj = Text()
    self.initUI()
    self.addWorker()

    self.toggleControl = False
    self.statusOkay = True

  def initUI(self):
    self.layout = QHBoxLayout()

    self.input = QLineEdit()
    self.layout.addWidget(self.input)
    self.input.textChanged.connect(self.obj.setText)
    self.obj.textChanged.connect(self.textChangedReceive)

    self.status = QLabel('0')
    self.layout.addWidget(self.status)

    self.control = QPushButton('Start', self)
    self.layout.addWidget(self.control)
    self.control.clicked.connect(self.toggleActive)

    self.setLayout(self.layout)
    self.resize(self.layout.sizeHint())
    self.setWindowTitle('Q_PROPERTY Demo')

  def addWorker(self):
    self.worker = Worker()
    self.worker.statusChanged_signal.connect(self.statusChangedReceived)
    self.start_signal.connect(self.worker.start)
    self.stop_signal.connect(self.worker.stop)
    self.setup_signal.connect(self.worker.setup_cmd_args)
    self.clear_error_signal.connect(self.worker.clear_error)
    self.worker.block_signal.connect(self.block)

    self.timer = QTimer()
    self.timer.setInterval(1000)
    self.timer.timeout.connect(self.worker.status)

    self.worker_thread = QThread(self)

    self.worker.moveToThread(self.worker_thread)
    self.timer.moveToThread(self.worker_thread)

#    self.worker_thread.finished.connect(self.worker_thread.deleteLater)
    self.worker_thread.finished.connect(self.worker.deleteLater)
    self.worker_thread.finished.connect(self.timer.deleteLater)

    self.worker_thread.started.connect(self.timer.start)
    self.worker_thread.start()

  def __del__(self):
    print('close event')
    if(self.worker_thread.isRunning()):
      print('thread running')
      self.worker_thread.exit()
      while(not self.worker_thread.isFinished()):
        pass
      print('thread done')

  @pyqtSlot(bool)
  def block(self, block_flag):
    self.control.setDisabled(block_flag)

  @pyqtSlot()
  def toggleActive(self):
    if not self.statusOkay:
      self.statusOkay = True
      print('Error acknowledged')
      self.clear_error_signal.emit()

    if not self.input.text(): return

    self.toggleControl = not self.toggleControl

    if self.toggleControl:
      self.setup_signal.emit(self.input.text())
      self.start_signal.emit()
      self.input.setDisabled(True)
    else:
      self.stop_signal.emit()

  @pyqtSlot(int)
  def statusChangedReceived(self, status):
    self.status.setText(str(status))

    if status == Worker.Status.inactive:
      self.control.setText('Start')
      self.input.setDisabled(False)
    if status == Worker.Status.running:
      self.control.setText('Stop')
#      self.input.setDisabled(True)
    elif status == Worker.Status.failed:
      print('An internal error has occurred. Please acknowledge the error to reset the state')
      self.control.setText('Acknowledge error')
      self.statusOkay = False
      self.input.setDisabled(False)

  @pyqtSlot(int)
  def textChangedReceive(self, cmd):
    if cmd == Text.CommandType.cmd_exit:
      self.close()
    elif cmd == Text.CommandType.cmd_hello:
      print('Hello there, stranger!')

def main():
  app = QApplication(argv)
  ex = Example()
  ex.show()
  exit(app.exec_())

if __name__ == '__main__':
  main()